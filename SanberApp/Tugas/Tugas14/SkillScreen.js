import React from 'react';
import { View,Image, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import data from './skillData.json';
import DataSkill from './dataSkill'

export default class App extends React.Component {
    render() {
    return (
        <View style={styles.container}>
            <StatusBar translucent ={false} backgroundColor={"white"}/>
            <View style={styles.logoview}>    
                <Image source={require('./images/logo.png')} style={styles.logo}/>
            </View>
            <View style={{flexDirection:'row',alignItems:'flex-start'}}>
                <Image source={require('./images/photo.jpg')} style={styles.photo}/>
                <View>
                    <Text style={{fontSize:12,marginLeft:11,color:'black'}}>Hai,</Text>
                    <Text style={{fontSize:16,marginLeft:11,color:'#003366'}}>Farhan Ramadhan</Text>
                </View>
            </View>
            <Text style={{paddingTop:16,fontSize:36,color:'#003366'}}>SKILL</Text>
            <View style={styles.categori}>
                <TouchableOpacity style={{width:125,height:32,alignItems:'center',justifyContent:'center',backgroundColor:'#B4E9FF',borderRadius:8}}>

                    <Text style={{fontSize:12,fontWeight:'bold'}}>Library/Framework</Text>

                </TouchableOpacity>
                <TouchableOpacity style={{width:136,height:32,alignItems:'center',justifyContent:'center',backgroundColor:'#B4E9FF',borderRadius:8}}>
                    
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Bahasa Pemrograman</Text>
                
                </TouchableOpacity>
                <TouchableOpacity style={{width:70,height:32,alignItems:'center',justifyContent:'center',backgroundColor:'#B4E9FF',borderRadius:8}}>
 
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Teknologi</Text>
                
                </TouchableOpacity>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><DataSkill skill={skill.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:8,backgroundColor:'transparent'}}/>}
                />
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingLeft: 16
    },
    logoview:{
        flexDirection : 'row',
        justifyContent:'flex-end',
    },
    photo:{
        width : 26,
        height:26,
        borderRadius:13
    },
    logo:{
        width: 187.5,
        height: 51,
    },
    body:{
        paddingTop:10,
        flex:1
    },
    categori:{
        borderTopWidth:2,
        borderTopColor:'#3EC6FF',
        backgroundColor:'white',
        width: 343,
        height:52,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    body:{
        flex:1
    }
})