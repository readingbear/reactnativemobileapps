import React, { Component } from 'react';
import {StyleSheet,Text,View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class DataSkill extends Component{
    render(){
        let skill = this.props.skill;
        return(
        <View style={styles.datacontainer}>
            <Icon name={skill.iconName} size={75} color='#003366' />
            <View>
                <Text style={{fontSize:24,fontWeight:'bold',color:'#003366'}}>{skill.skillName}</Text>
                <Text style={{fontSize:16,fontWeight:'bold',color:'#3EC6FF'}}>{skill.categoryName}</Text>
                <Text style={{fontSize:48,fontWeight:'bold',color:'white',alignSelf:'flex-end'}}>{skill.percentageProgress}</Text>
            </View>
            <Icon name='chevron-right' size={75} color='#003366' />    
        </View>
        )
    }
}

const styles = StyleSheet.create({
    datacontainer:{
        flex:1,
        width: 343,
        height: 129,
        backgroundColor:'#B4E9FF',
        borderRadius:8,
        paddingHorizontal:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'  
    }
})