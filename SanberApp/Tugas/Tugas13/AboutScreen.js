import React,{Component} from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
    render() {
        return(
            <View style ={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                <Text style ={{fontSize:36,color:'#003366',textAlign:'center'}}>Tentang Saya</Text>
                <View style={{alignItems:'center', paddingTop: 12}}>
                    <Image source={require('./images/photo.jpg')} style={styles.photoprofile}/>
                    <Text style={styles.name}>Farhan Ramadhan</Text>
                    <Text style={styles.work}>React Native Developer</Text>
                </View>
                <View style={styles.portofolio}>
                    <Text style={styles.info}>Portofolio</Text>
                    <View style={styles.portliobox}>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="gitlab" size={43} color= '#3EC6FF'/>
                            <Text style={styles.icons}>@readingbear</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="gitlab" size={43} color= '#3EC6FF'/>
                            <Text style={styles.icons}>@readingbear</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.accsocial}>
                    <Text style={styles.info}>Hubungi Saya</Text>
                    <View style={styles.portliobox1}>
                        <TouchableOpacity style={styles.tabItem2}>
                            <Icon name="facebook" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>Farhan Rmd</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem1}>
                            <Icon name="instagram" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>@farhan.rmd</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem1}>
                            <Icon name="twitter" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>@farhan_rmdhan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop : 64,
        flex: 1,
        backgroundColor: "white",
    },
    photoprofile:{
        width: 200, 
        height: 200,
        borderRadius:100
    },
    name:{
        paddingTop:24,
        fontSize:24,
        color:'#003366'
    },
    work:{
        paddingTop:8,
        fontSize:16,
        color:'#3EC6FF'
    },
    portofolio:{
        paddingTop:21,
        paddingHorizontal:16
    },
    accsocial:{
        paddingTop:9,
        paddingHorizontal:16
    },
    portliobox:{
        width:351,
        height:106,
        borderTopWidth:1,
        borderTopColor:'#003366',
        paddingHorizontal:56,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    portliobox1:{
        width:351,
        height:215,
        borderTopWidth:1,
        borderTopColor:'#003366',
        alignItems: 'center',
        justifyContent: 'center'
    },
    info:{
        fontSize:18,
        color:'#003366'
    },
    icons:{
        paddingTop:10,
        fontSize : 16,
        color:'#003366'
    },
    icons1:{
        paddingHorizontal:19,
        fontSize : 16,
        color:'#003366'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItem1: {
        paddingTop:30,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    tabItem2: {
        paddingTop:19,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})