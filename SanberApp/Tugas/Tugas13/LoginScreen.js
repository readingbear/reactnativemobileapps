import React,{Component} from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput } from 'react-native';
import {StatusBar} from 'expo-status-bar';

export default class App extends Component {
    render() {
        return(
            <View  style={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                <View style={{alignItems:'center'}}>
                    <Image source ={require('./images/logo.png')} style ={styles.imgStyle} />
                    <Text style={{fontSize: 24, color: '#003366',paddingTop:70,paddingBottom:24}}>Login</Text>
                </View>
                <View style={styles.inputstyle}>
                    <Text style={styles.txtstyle}>Username</Text>
                    <TextInput style={styles.boxstyle}/>
                    <Text style={styles.txtstyle}>Pasword</Text>
                    <TextInput style={styles.boxstyle} secureTextEntry={true}/>
                </View>
                <View style={{alignItems:'center',paddingTop:32,paddingBottom:16}}>
                    <TouchableOpacity style={styles.buttonstyle}>
                        <Text style={styles.txtbutton}>Masuk</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{textAlign:'center',color:'#3EC6FF',fontSize:24}}>atau</Text>
                <View style={{alignItems:'center',paddingTop:16}}>
                    <TouchableOpacity style={styles.buttonstyle1}>
                        <Text style={styles.txtbutton}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop : 63,
        flex: 1,
        backgroundColor: "white",
    },
    imgStyle:{
        width:375,
        height:102
    },
    inputstyle:{
        paddingHorizontal: 50
    },
    txtstyle:{
        fontSize: 16, 
        color: '#003366',
        paddingTop: 16,
        paddingBottom: 4
    },
    boxstyle:{
        width:294,
        height:48,
        backgroundColor:'white',
        borderWidth:2,
        borderColor:'#003366',
        paddingHorizontal:10
    },
    buttonstyle:{
        width:140,
        height:40,
        backgroundColor:'#3EC6FF',
        borderRadius: 25,
        alignContent: 'center',
        paddingVertical:2
    },
    txtbutton:{
        fontSize:24,
        color:'white',
        textAlign:'center'
    },
    buttonstyle1:{
        width:140,
        height:40,
        backgroundColor:'#003366',
        borderRadius: 25,
        alignContent: 'center',
        paddingVertical:2
    },
})